package ru.buzanov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.buzanov.tm.entity.Roles;
import ru.buzanov.tm.enumerated.RoleType;

import java.util.Collection;

@Repository
public interface RoleRepository extends JpaRepository<Roles, String> {
    Collection<Roles> findAllByUserId(String userId);
    Collection<Roles> findAllByRoleType(RoleType roleType);

}
