package ru.buzanov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.buzanov.tm.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    boolean existsByLogin(String login);

    User findByLogin(String login);

}