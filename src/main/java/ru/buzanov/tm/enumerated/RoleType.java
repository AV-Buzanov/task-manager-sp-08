package ru.buzanov.tm.enumerated;

import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.XmlEnumValue;

public enum RoleType {
    @XmlEnumValue(value = "Администратор")
    ADMIN("Администратор"),
    @XmlEnumValue(value = "Пользователь")
    USER("Пользователь");

    @NotNull private String name;
    RoleType(@NotNull final String name) {
        this.name = name;
    }

    @NotNull
    public String displayName() {
        return name;
    }
}
