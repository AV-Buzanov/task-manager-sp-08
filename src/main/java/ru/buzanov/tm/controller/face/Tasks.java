package ru.buzanov.tm.controller.face;

import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.primefaces.PrimeFaces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;
import ru.buzanov.tm.dto.ProjectDTO;
import ru.buzanov.tm.dto.TaskDTO;
import ru.buzanov.tm.security.CustomUser;
import ru.buzanov.tm.service.ProjectService;
import ru.buzanov.tm.service.TaskService;
import ru.buzanov.tm.service.UserService;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import java.util.Collection;
import java.util.List;

@Component("tasks")
//@Scope("view")
@SessionScope
@ELBeanName(value = "tasks")
@Join(path = "/tasks", to = "/faces/taskface.xhtml")
public class Tasks {
    @Autowired
    private TaskService taskService;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private UserService userService;

    private String userId;

    private TaskDTO selectedTask;

    private List<TaskDTO> filteredTasks;

    @PostConstruct
    public void init() {
        final CustomUser customUser = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        userId = customUser.getUserId();
    }

    public Collection<TaskDTO> list() throws Exception {
        return taskService.findAll(getUserId());
    }

    public void save() throws Exception {
        try {
            taskService.merge(getUserId(), selectedTask.getId(), selectedTask);
        } catch (Exception e) {
            PrimeFaces.current().dialog().showMessageDynamic(new FacesMessage(e.getMessage()));
        } finally {
            selectedTask = null;
        }
    }

    public ProjectDTO project(String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty())
            return null;
        return projectService.findOne(userId, projectId);
    }

    public void remove() throws Exception {
        taskService.remove(getUserId(), selectedTask.getId());
    }

    public void create() throws Exception {
        final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setUserId(userId);
        selectedTask = taskDTO;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public TaskDTO getSelectedTask() {
        return selectedTask;
    }

    public void setSelectedTask(TaskDTO selectedTask) {
        this.selectedTask = selectedTask;
    }

    public List<TaskDTO> getFilteredTasks() {
        return filteredTasks;
    }

    public void setFilteredTasks(List<TaskDTO> filteredTasks) {
        this.filteredTasks = filteredTasks;
    }
}
