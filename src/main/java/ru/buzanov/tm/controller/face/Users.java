package ru.buzanov.tm.controller.face;

import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.primefaces.PrimeFaces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;
import ru.buzanov.tm.dto.UserDTO;
import ru.buzanov.tm.dto.TaskDTO;
import ru.buzanov.tm.enumerated.RoleType;
import ru.buzanov.tm.enumerated.Status;
import ru.buzanov.tm.security.CustomUser;
import ru.buzanov.tm.service.ProjectService;
import ru.buzanov.tm.service.TaskService;
import ru.buzanov.tm.service.UserService;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component("users")
//@Scope("view")
@SessionScope
@ELBeanName(value = "users")
@Join(path = "/users", to = "/faces/userface.xhtml")
public class Users {
    @Autowired
    private TaskService taskService;
    @Autowired
    private UserService userService;

    private String userId;

    private UserDTO selectedUser;

    private List<UserDTO> filteredUsers;


    @PostConstruct
    public void init() throws Exception {
        final CustomUser customUser = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        userId = customUser.getUserId();
    }

    public Collection<UserDTO> list() throws Exception {
        return userService.findAll();
    }

    public Collection<TaskDTO> tasks() throws Exception {
        return taskService.findByProjectId(getUserId(), selectedUser.getId());
    }

    public void save() throws Exception {
        try {
            userService.merge(selectedUser.getId(), selectedUser);
        } catch (Exception e) {
            PrimeFaces.current().dialog().showMessageDynamic(new FacesMessage(e.getMessage()));
        } finally {
            selectedUser = null;
        }
    }

    public void create() throws Exception {
        final UserDTO projectDTO = new UserDTO();
        selectedUser = projectDTO;
    }

    public void remove() throws Exception {
        userService.remove(selectedUser.getId());
    }

    public List<RoleType> roles() {
        final List<RoleType> roles = new ArrayList<>();
        for (RoleType s : RoleType.values())
            roles.add(s);
        return roles;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public UserDTO getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(UserDTO selectedUser) {
        this.selectedUser = selectedUser;
    }

    public List<UserDTO> getFilteredUsers() {
        return filteredUsers;
    }

    public void setFilteredUsers(List<UserDTO> filteredUsers) {
        this.filteredUsers = filteredUsers;
    }
}
