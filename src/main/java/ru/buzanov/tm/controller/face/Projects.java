package ru.buzanov.tm.controller.face;

import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.primefaces.PrimeFaces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;
import ru.buzanov.tm.dto.ProjectDTO;
import ru.buzanov.tm.dto.TaskDTO;
import ru.buzanov.tm.enumerated.Status;
import ru.buzanov.tm.security.CustomUser;
import ru.buzanov.tm.service.ProjectService;
import ru.buzanov.tm.service.TaskService;
import ru.buzanov.tm.service.UserService;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component("projects")
//@Scope("view")
@SessionScope
@ELBeanName(value = "projects")
@Join(path = "/projects", to = "/faces/projectface.xhtml")
public class Projects {
    @Autowired
    private ProjectService projectService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private UserService userService;

    private String userId;

    private ProjectDTO selectedProject;

    private List<ProjectDTO> filteredProjects;


    @PostConstruct
    public void init() throws Exception {
        final CustomUser customUser = (CustomUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        userId = customUser.getUserId();
    }

    public Collection<ProjectDTO> list() throws Exception {
        return projectService.findAll(getUserId());
    }

    public Collection<TaskDTO> tasks() throws Exception {
        return taskService.findByProjectId(getUserId(), selectedProject.getId());
    }

    public void save() throws Exception {
        try {
            projectService.merge(getUserId(), selectedProject.getId(), selectedProject);
        } catch (Exception e) {
            PrimeFaces.current().dialog().showMessageDynamic(new FacesMessage(e.getMessage()));
        } finally {
            selectedProject = null;
        }
    }

    public void create() throws Exception {
        final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setUserId(userId);
        selectedProject = projectDTO;
    }

    public void remove() throws Exception {
        projectService.remove(getUserId(), selectedProject.getId());
    }

    public List<Status> statuses() {
        final List<Status> statuses = new ArrayList<>();
        for (Status s : Status.values())
            statuses.add(s);
        return statuses;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ProjectDTO getSelectedProject() {
        return selectedProject;
    }

    public void setSelectedProject(ProjectDTO selectedProject) {
        this.selectedProject = selectedProject;
    }

    public List<ProjectDTO> getFilteredProjects() {
        return filteredProjects;
    }

    public void setFilteredProjects(List<ProjectDTO> filteredProjects) {
        this.filteredProjects = filteredProjects;
    }
}
