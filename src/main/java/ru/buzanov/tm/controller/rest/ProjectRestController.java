package ru.buzanov.tm.controller.rest;

import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.buzanov.tm.dto.ProjectDTO;
import ru.buzanov.tm.security.CustomUser;
import ru.buzanov.tm.service.ProjectService;

import java.util.Collection;

@NoArgsConstructor
@RestController
@PreAuthorize("isAuthenticated()")
@RequestMapping(value = "/rest/project")
public class ProjectRestController {
    @Autowired
    private ProjectService projectService;

    @RequestMapping(value = "/findAll", method = RequestMethod.GET)
    public ResponseEntity<Collection<ProjectDTO>> listProject(@AuthenticationPrincipal CustomUser user) throws Exception {
        return ResponseEntity.ok(projectService.findAll(user.getUserId()));
    }

    @RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
    public ResponseEntity<ProjectDTO> getProject(@AuthenticationPrincipal CustomUser user, @PathVariable final String id) throws Exception {
        return ResponseEntity.ok(projectService.findOne(user.getUserId(), id));
    }

    @RequestMapping(value = "/mergeAll", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<ProjectDTO>> mergeAllProject(@AuthenticationPrincipal CustomUser user, @RequestBody Collection<ProjectDTO> projects) throws Exception {
        for (ProjectDTO project : projects)
            projectService.merge(user.getUserId(), project.getId(), project);
        return ResponseEntity.ok(projectService.findAll(user.getUserId()));
    }

    @RequestMapping(value = "/merge", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProjectDTO> mergeProject(@AuthenticationPrincipal CustomUser user, @RequestBody ProjectDTO project) throws Exception {
        projectService.merge(user.getUserId(), project.getId(), project);
        return ResponseEntity.ok(projectService.findOne(user.getUserId(), project.getId()));
    }

    @RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
    public ResponseEntity removeProject(@AuthenticationPrincipal CustomUser user, @PathVariable final String id) throws Exception {
        projectService.remove(user.getUserId(), id);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/removeAll", method = RequestMethod.DELETE)
    public ResponseEntity removeAllProject(@AuthenticationPrincipal CustomUser user) throws Exception {
        projectService.removeAll(user.getUserId());
        return ResponseEntity.ok().build();
    }
}