package ru.buzanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.buzanov.tm.dto.ProjectDTO;
import ru.buzanov.tm.enumerated.Field;
import ru.buzanov.tm.security.CustomUser;
import ru.buzanov.tm.service.ProjectService;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@Service
public class ProjectEndpoint {
    @Autowired
    public ProjectService projectService;

    @WebMethod
    @Nullable
    public Collection<ProjectDTO> findAllP() throws Exception {
        CustomUser user;
        if ((user = getUser()) == null)
            return null;
        return projectService.findAll(user.getUserId());
    }

    @WebMethod
    @Nullable
    public ProjectDTO findOneP(@Nullable final String id) throws Exception {
        CustomUser user;
        if ((user = getUser()) == null)
            return null;
        return projectService.findOne(user.getUserId(), id);
    }

    @WebMethod
    public boolean isNameExistP(@Nullable final String name) throws Exception {
        CustomUser user;
        if ((user = getUser()) == null)
            return false;
        return projectService.isNameExist(user.getUserId(), name);
    }

    @WebMethod
    @Nullable
    public String getListP() throws Exception {
        CustomUser user;
        if ((user = getUser()) == null)
            return null;
        return projectService.getList(user.getUserId());
    }

    @WebMethod
    @Nullable
    public String getIdByCountP(int count) throws Exception {
        CustomUser user;
        if ((user = getUser()) == null)
            return null;
        return projectService.getIdByCount(user.getUserId(), count);
    }

    public void mergeP(@Nullable final String id, @Nullable final ProjectDTO project) throws Exception {
        CustomUser user;
        if ((user = getUser()) == null)
            return;
        projectService.merge(user.getUserId(), id, project);
    }

    @WebMethod
    @Nullable
    public ProjectDTO removeP(@Nullable final String id) throws Exception {
        CustomUser user;
        if ((user = getUser()) == null)
            return null;
        projectService.remove(user.getUserId(), id);
        return null;
    }

    public void removeAllP() throws Exception {
        CustomUser user;
        if ((user = getUser()) == null)
            return;
        projectService.removeAll(user.getUserId());
    }

    @WebMethod
    @Nullable
    public Collection<ProjectDTO> findByDescriptionP(@Nullable final String desc) throws Exception {
        CustomUser user;
        if ((user = getUser()) == null)
            return null;
        return projectService.findByDescription(user.getUserId(), desc);
    }

    @WebMethod
    @Nullable
    public Collection<ProjectDTO> findByNameP(@Nullable final String name) throws Exception {
        CustomUser user;
        if ((user = getUser()) == null)
            return null;
        return projectService.findByName(user.getUserId(), name);
    }

    @WebMethod
    public @Nullable Collection<ProjectDTO> findAllOrderedP(boolean dir, @NotNull Field field) throws Exception {
        CustomUser user;
        if ((user = getUser()) == null)
            return null;
        return projectService.findAllOrdered(user.getUserId(), dir, field);
    }

    @WebMethod
    @Nullable
    public ProjectDTO loadP(@Nullable ProjectDTO entity) throws Exception {
        CustomUser user;
        if ((user = getUser()) == null)
            return null;
        projectService.load(user.getUserId(), entity);
        return null;
    }

    @WebMethod
    public void loadListP(List<ProjectDTO> list) throws Exception {
        CustomUser user;
        if ((user = getUser()) == null)
            return;
        projectService.load(user.getUserId(), list);
    }

    @Nullable
    private CustomUser getUser() {
        if (SecurityContextHolder.getContext().getAuthentication() == null)
            return null;
        final Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (user instanceof CustomUser)
            return (CustomUser) user;
        return null;
    }
}
