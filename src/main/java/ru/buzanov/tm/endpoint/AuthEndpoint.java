package ru.buzanov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.buzanov.tm.dto.UserDTO;
import ru.buzanov.tm.enumerated.RoleType;
import ru.buzanov.tm.security.CustomUser;
import ru.buzanov.tm.service.UserService;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
@Service
public class AuthEndpoint {
    @Autowired
    public UserService userService;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @WebMethod
    public void auth(final String login, final String password) throws Exception {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(login, password);
        Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @WebMethod
    public void logOut() throws Exception {
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    @WebMethod
    public UserDTO profile() throws Exception {
        if (SecurityContextHolder.getContext().getAuthentication() == null)
            return null;
        Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (user instanceof CustomUser)
            return userService.findOne(((CustomUser) user).getUserId());
        return null;
    }

    @WebMethod
    public void remove() throws Exception {
        if (SecurityContextHolder.getContext().getAuthentication() == null)
            return;
        Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (user instanceof CustomUser) {
            SecurityContextHolder.getContext().setAuthentication(null);
            userService.remove(((CustomUser) user).getUserId());
        }
    }

    @WebMethod
    public void register(final String login, final String password) throws Exception {
        final UserDTO userDTO = new UserDTO();
        userDTO.setPasswordHash(passwordEncoder.encode(password));
        userDTO.setLogin(login);
        userDTO.getRoles().add(RoleType.USER);
        userService.load(userDTO);
    }
}
