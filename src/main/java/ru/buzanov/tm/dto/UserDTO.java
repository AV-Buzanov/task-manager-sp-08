package ru.buzanov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.enumerated.RoleType;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class UserDTO extends AbstractDto {
    @Nullable
    private String name;
    @Nullable
    private String login;
    @Nullable
    private String passwordHash;
    @NotNull
    private List<RoleType> roles = new ArrayList<>();
}
